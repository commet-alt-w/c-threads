#toolchain.cmake
# cmake toolchain file

# c compiler and standard
set(CMAKE_C_COMPILER "/usr/bin/gcc")
set(CMAKE_C_STANDARD "99")
set(CMAKE_C_STANDARD_REQUIRED YES)

# linker flags
set(CMAKE_EXE_LINKER_FLAGS_RELEASE_INIT "-Wl,--strip-debug")

# global cflags
set(CMAKE_C_FLAGS_INIT "-Wall -Wextra -Wunreachable-code -Wpedantic")

# cflags for debug build
set(CMAKE_C_FLAGS_DEBUG_INIT "-O0 -D_DEBUG -g3 -ggdb3")

# cflags for release build
set(CMAKE_C_FLAGS_RELEASE_INIT "-O2 -g -DNDEBUG -s")
