#include "pthread-cleanup.h"
#include "pthread-state-test.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main(void) {
  // print args
  printf("c-threads\n\n");

  printf("pthread_cleanup_test\n");
  pthread_cleanup_test();

  printf("pthread_state_test\n");
  pthread_state_test();
  
  exit(EXIT_SUCCESS);
}
