#include "pthread-state-test.h"
#include "pthread-state.h"
#include <pthread.h>
#include <unistd.h>

void pthread_state_test(void) {
  pthread_t p1;
  pthread_t p2;
  int i1 = 1;
  int i2 = 2;
  
  pthread_create(&p1, NULL, pthread_state_handler, &i1);
  pthread_create(&p2, NULL, pthread_state_handler, &i2);

  sleep(10);

  pthread_cancel(p1);
  pthread_join(p1, NULL);
  pthread_cancel(p2);
  pthread_join(p2, NULL);
}
