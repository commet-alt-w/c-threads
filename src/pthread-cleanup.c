#include "pthread-cleanup.h"
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

static void
pthr_cleanup_handler(void *arg) {
  printf("pthr clean up handler\n");
  fflush(stdout);
}

static void *
pthr_handler(void *args) {
  // add cleanup handler
  pthread_cleanup_push(pthr_cleanup_handler, NULL);

  // main loop for thread
  while(1) {
    printf("pthr handler running\n");
    fflush(stdout);
    sleep(5);
  }

  // unreachable code
  pthread_cleanup_pop(1);
  
  return args;
}

void pthread_cleanup_test() {
  pthread_t pthr;
  // start thread
  pthread_create(&pthr, NULL, pthr_handler, NULL);
  sleep(10);
  // cancel thread
  pthread_cancel(pthr);
  // need to join threads waiting for cancel
  pthread_join(pthr, NULL);
}
