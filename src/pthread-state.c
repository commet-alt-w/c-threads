#include "pthread-state.h"
#include <unistd.h>
#include <stdio.h>

static int shared_int = 0;

static void
process_data(int thread_num) {
  int data = 0;

  while(1) {
    printf("thread_num %d: data = %d, shared_int = %d\n", thread_num, data, shared_int);
    fflush(stdout);
    data++;
    shared_int++;
    sleep(1);
  }
  
}

void *
pthread_state_handler(void *args) {
  // get thread number
  int *i = args;
  int thread_num = *i;
  
  process_data(thread_num);
  
  return args;
}
